import 'package:flutter/material.dart';

class MyStatelessWidget extends StatelessWidget {
  const MyStatelessWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('LayoutBuilder Example')),
      body: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
        //showing actual width
        print('actual widdh:: \t');
        print(constraints.maxWidth);
        print("Scaffold has appBar?\t ::");
        print(Scaffold.of(context).hasAppBar); //to access to parents constraints
        //access constraints using @MediaQuert
        var screenSize = MediaQuery.of(context).size;
        print("orientation:: " + screenSize.width.toString());
        var orientation = MediaQuery.of(context).orientation;
        print("orientation ::\t" + orientation.toString());

        if (constraints.maxWidth > 600) {
          return _buildWideContainer();
        } else {
          return _buildNormalContainer();
        }
      }),
    );
  }

  Widget _buildNormalContainer() {
    return Center(
      child: Container(
        height: 100.0,
        width: 100.0,
        color: Colors.red,
      ),
    );
  }

  Widget _buildWideContainer() {
    return Center(
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
      Container(
        height: 100.0,
        width: 100.0,
        color: Colors.red,
      ),
      Container(
        height: 100.0,
        width: 100.0,
        color: Colors.green,
      ),
    ]));
  }
}
