import 'package:flutter/material.dart';

class ListViewWidget extends StatelessWidget {
  const ListViewWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      _buildTile("java", "subtitle 1", Icons.theaters),
      _buildTile("java", "subtitle 1", Icons.restaurant),
      _buildTile("java", "subtitle 1", Icons.restaurant),
    ]);
  }

  ListTile _buildTile(String title, String subtitle, IconData icon) {
    return ListTile(
        title: Text(title,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 20,
            )),
        subtitle: Text(subtitle),
        leading: Icon(icon, color: Colors.blue[50]));
  }
}
