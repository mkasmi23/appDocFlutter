import 'package:flutter/material.dart';

class Grid extends StatelessWidget {
  const Grid({Key? key}) : super(key: key);

  List<Container> buildGridList(int count) => List.generate(count, (index) => Container(child: Image.asset('images/$index.jpg')));
  @override
  Widget build(BuildContext context) {
    return GridView.extent(maxCrossAxisExtent: 300, padding: const EdgeInsets.all(4), mainAxisSpacing: 4, crossAxisSpacing: 4, children: buildGridList(3));
  }
}
