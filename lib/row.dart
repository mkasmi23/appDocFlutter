import 'package:flutter/cupertino.dart';

var image1 = Image.asset('images/mk.jpg');
var image2 = Image.asset('images/mk.jpg');
var image3 = Image.asset('images/mk.jpg');

class RowUI extends StatelessWidget {
  const RowUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: image1,
        ),
        Expanded(
          child: image2,
          flex: 2,
        ),
        Expanded(
          child: image3,
        )
      ],
    );
  }
}
